
const key_fill_store_font_items = [{ key: "key_fill_bible_font", id: "#bibleFont" }, { key: "key_fill_song_font", id: "#songFont" }, { key: "key_fill_timer_font", id: "#timerFont" }];
const key_fill_store_keys = ["key_fill_bible_css", "key_fill_song_css", "key_fill_timer_css"];
const chroma_store_font_items = [{ key: "chroma_bible_font", id: "#bibleFont" }, { key: "chroma_song_font", id: "#songFont" }, { key: "chroma_timer_font", id: "#timerFont" }];
const chroma_store_keys = ["chroma_bible_css", "chroma_song_css", "chroma_timer_css"];
var curentSlideType = "SONG";
var openLpContentShowing = false;

function applyStoredCss(store_key) {
  let store_css = window.localStorage.getItem(store_key);
  if (store_css) {
    let css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = store_css;
    document.head.appendChild(css);
  }
}

function applyStoredFont(item) {
  let font = window.localStorage.getItem(item.key);
  if (font) {
    let css_text = item.id + "{ font-family: " + font + ", monospace}";
    let css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = css_text;
    document.head.appendChild(css);
  }
}

function refreshCss(type) {
  document.querySelectorAll('style').forEach(item => item.remove())
  if (type == "key_fill") {
    key_fill_store_font_items.forEach(function (item, index) {
      applyStoredFont(item);
    });
    key_fill_store_keys.forEach(function (item, index) {
      applyStoredCss(item);
    });
  } else if (type == "chroma") {
    chroma_store_font_items.forEach(function (item, index) {
      applyStoredFont(item);
    });
    chroma_store_keys.forEach(function (item, index) {
      applyStoredCss(item);
    });
  }
}

function showOpenLpContent() {
  var song_els = document.querySelectorAll('*[id^="song"]');
  var bible_els = document.querySelectorAll('*[id^="bible"]');
  if (curentSlideType == "BIBLE") {
    song_els.forEach(function (item, index) {
      item.style.display = "none";
    });
    bible_els.forEach(function (item, index) {
      item.style.display = "inline";
    });
  } else if (curentSlideType == "SONG") {
    bible_els.forEach(function (item, index) {
      item.style.display = "none";
    });
    song_els.forEach(function (item, index) {
      item.style.display = "inline";
    });
  }
  openLpContentShowing = true;
}

function hideOpenLpContent() {
  var song_els = document.querySelectorAll('*[id^="song"]');
  var bible_els = document.querySelectorAll('*[id^="bible"]');
  song_els.forEach(function (item, index) {
    item.style.display = "none";
  });
  bible_els.forEach(function (item, index) {
    item.style.display = "none";
  });

  openLpContentShowing = false;
}

// *** We use broadcast channels to send content and commands from
//     the python context (via main.js) and from Vue App
//     Here we have listeners to react to these data and commands
const timer_channel = new BroadcastChannel('timer_channel')
timer_channel.onmessage = (event) => {
  document.getElementById("timerText").innerHTML = event.data;
}

const control_channel = new BroadcastChannel('control_channel')
control_channel.onmessage = (event) => {
  let mangled_id = ((document.head.id == "key" || document.head.id == "fill") ? "key_fill" : document.head.id);
  if (event.data.window_id == mangled_id) {
    if (event.data.action == "setTimerVisibility") {
      let value = (event.data.value ? "inline" : "none");
      document.getElementById("timerText").style.display = value;
      document.getElementById("timerBackground").style.display = value;
    } else if (event.data.action == "setOpenLpVisibility") {
      if (event.data.value) {
        showOpenLpContent();
      } else {
        hideOpenLpContent();
      }
    }
  }
}

const openlp_channel = new BroadcastChannel('openlp_channel')
openlp_channel.onmessage = (event) => {
  let mangled_id = ((document.head.id == "key" || document.head.id == "fill") ? "key_fill" : document.head.id);
  if (event.data.window_id == mangled_id) {
    if (event.data.action == "content") {
      // Update both types of content - one will always be hidden
      document.getElementById('songText').innerHTML = event.data.value.current
      document.getElementById('songTitle').innerHTML = event.data.value.title
      document.getElementById('songFooter').innerHTML = event.data.value.footer
      document.getElementById('bibleText').innerHTML = event.data.value.current
      document.getElementById('bibleTitle').innerHTML = event.data.value.title
    }
    else if (event.data.action == "change_slide_type") {
      if (curentSlideType != event.data.value) {
        curentSlideType = event.data.value;

        if (openLpContentShowing) {
          // Calling show again will sort out type
          showOpenLpContent();
        }
      }
    }
  }
}

// On inital loading of the window, apply whatever current styling is in our localStorage
document.addEventListener('DOMContentLoaded', function (event) {
  let mangled_id = ((document.head.id == "key" || document.head.id == "fill") ? "key_fill" : document.head.id);
  refreshCss(mangled_id);

  // And make the openlp content visible by default as per the defualt in ControlPanel.vue
  // openlp_visible data element
  showOpenLpContent();
})

// This allows us to just write to local storage in Vue app and have the
// changes automatically applied to our window.  Nice :-)
window.onstorage = (e) => {
  // Only interested in our own keys... filter anyone elses
  let re = /^(key_fill|chroma)_[a-z]+_(css|font)$/;
  let match = e.key.match(re);
  if (match) {
    let mangled_id = ((document.head.id == "key" || document.head.id == "fill") ? "key_fill" : document.head.id);
    if (mangled_id == match[1]) {
      refreshCss(mangled_id);
    }
  }
};
