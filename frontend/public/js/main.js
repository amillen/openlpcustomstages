// ------------------------------------------------------------------------
//   Triggers to other windows (key/fill) to perform an action or change
// ------------------------------------------------------------------------
const openlp_channel = new BroadcastChannel('openlp_channel')


// ------------------------------------------------------------------------
//   Event listeners
// ------------------------------------------------------------------------
window.addEventListener('pywebviewready', function () {

  // ready - fetch initial values for the python domain
  let host_id = window.localStorage.getItem("openlp_host_id");
  if (host_id) {
    pywebview.api.setOpenLPHost(host_id);
  }

  // Pass our locally calculated list over to Vue where it get's
  // merged with a default list and checked for validity
  pywebview.api.getSystemFontList().then((response) => {
    window.eventBus.$emit("system-fonts", JSON.parse(response));
  });
});

// This allows us to just write to local storage in Vue app and have the
// changes automatically applied to our window.  Nice :-)
window.onstorage = (e) => {
  // Only interested in our own keys... filter anyone elses
  if (e.key == "openlp_host_id") {
    pywebview.api.setOpenLPHost(e.newValue);
  }
};
