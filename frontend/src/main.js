import Vue from "vue";
import App from "./App.vue";
import Buefy from "buefy";
import "buefy/dist/buefy.css";
import VueLocalStorage from "vue-localstorage";
import dedent from "dedent-js";

Vue.use(VueLocalStorage);

Vue.use(Buefy, {
  defaultIconPack: "fas",
  defaultContainerElement: "#content",
});

Vue.config.productionTip = false;

Vue.prototype.$eventBus = new Vue(); // Global event bus
Vue.prototype.$bc_timer = new BroadcastChannel("timer_channel");
Vue.prototype.$bc_control = new BroadcastChannel("control_channel");
Vue.prototype.$bc_openlp = new BroadcastChannel("openlp_channel");

window.eventBus = Vue.prototype.$eventBus;

new Vue({
  render: (h) => h(App),
  localStorage: {
    timer_target: {
      type: Object,
      default: { hours: 11, minutes: 30 },
    },
    key_fill_timer_font: {
      type: String,
      default: "Courier",
    },
    chroma_timer_font: {
      type: String,
      default: "Courier",
    },
    key_fill_timer_css: {
      type: String,
      default: dedent(`
                #timerText {
                  bottom: 0px;
                  width: 4em;
                  text-align: center;
                  font-size: 7vh;
                  color: #fff;
                  background: #222;
                }
                `),
    },
    chroma_timer_css: {
      type: String,
      default: dedent(`
                #timerText {
                  top: 50%;
                  left: 50%;
                  transform: translate(-50%, -50%);
                  width: 8ch;
                  text-align: center;
                  font-size: 80px;
                  color: #fff;
                  background: rgba(0, 0, 0, 0.2);
                }
                `),
    },
    key_fill_song_font: {
      type: String,
      default: "Arial",
    },
    chroma_song_font: {
      type: String,
      default: "Arial",
    },
    key_fill_song_css: {
      type: String,
      default: dedent(`
                #songText {
                  color: white;
                  bottom: 0px;
                  padding: 2vh;
                  width: 100%;
                  min-height: 2.2em;
                  line-height: 1.1em;
                  font-size: 7.25vh;
                  text-shadow: 2px 2px black;
                  text-align: center;
                  background-image: linear-gradient(to bottom, rgba(0,0,0,0.35), rgba(0,0,0,1));
                }
                #songFooter {
                  color: white;
                  font-size: 1.3vh;
                  right: 0px;
                  bottom: 0px;
                }
                #songTitle {
                  display: none !important;
                }
                `),
    },
    chroma_song_css: {
      type: String,
      default: dedent(`
                #songText {
                  padding-top: 5vh;
                  font-size: 8.25vh;
                  color: white;
                  text-align: center;
                  width: 100%;
                }

                #songTitle {
                  display: none !important;
                }

                #songFooter {
                  color: white;
                  font-size: 1.75vh;
                  right: 0px;
                  bottom: 0px;
                }
                `),
    },
    key_fill_bible_font: {
      type: String,
      default: "Arial",
    },
    chroma_bible_font: {
      type: String,
      default: "Arial",
    },
    key_fill_bible_css: {
      type: String,
      default: dedent(`
                #bibleTitle {
                  top: calc(80% - 5vh);
                  font-size: 4vh;
                  font-weight: bold;
                  color: white;
                  min-height: 4vh;
                  margin-left: 5vw;
                  padding: 0.3vh 2vw;
                  background-color: black;
                  max-width: max-content;
                }
                #bibleText {
                  top: 80%;
                  font-size: 2.6vh;
                  color: white;
                  padding: 2vh 1vw;
                  background: linear-gradient(to right, #e66465, #9198e5);
                  max-width: 45vw;
                  margin-left: 5vw;
                }
                `),
    },
    chroma_bible_css: {
      type: String,
      default: dedent(`
                #bibleTitle {
                  top: calc(80% - 5vh);
                  font-size: 4vh;
                  font-weight: bold;
                  color: white;
                  min-height: 4vh;
                  margin-left: 5vw;
                  padding: 0.3vh 2vw;
                  background-color: black;
                  max-width: max-content;
                }
                #bibleText {
                  top: 80%;
                  font-size: 2.6vh;
                  color: white;
                  padding: 2vh 1vw;
                  background: linear-gradient(to right, #160425, #9138e5);
                  max-width: 45vw;
                  margin-left: 5vw;
                }
                `),
    },
  },
}).$mount("#app");
