"""OpenLP Alternate Stage Screens

Usage:
  olpass.py [options]
  olpass.py --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  --debug       Enable debug output
"""
import webview
import logging
import re
import json
import win32gui
from docopt import docopt
from openlp.openlp import OpenLP, Slide
from openlp.openlp import SlideType

# Change top level logger format and level
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)s %(levelname)s: %(message)s')

# create our own logger
app_log = logging.getLogger(__name__)


class MainApi:
    KEY_FILL = "key_fill"
    CHROMA = "chroma"

    def __init__(self):
        self._key = None
        self._fill = None
        self._chroma = None
        self._key_fill_upper = False
        self._chroma_upper = False
        self._inc = 0
        self._main_window = None
        self._app_closing = False

        self._key_hidden = False
        self._fill_hidden = False
        self._chroma_hidden = False

        self.olp = OpenLP()
        self.olp.registerSlideChangeCallback(self._slide_updated)

        self._last_slide_type = SlideType.UNKNOWN

    def set_main_window(self, window):
        self._main_window = window

    def getOpenLPHost(self):
        return self.olp._host

    def setOpenLPHost(self, host):
        self.olp._host = host
        app_log.debug(host)

    def on_fill_closed(self):
        if self._fill is not None:
            app_log.debug('fill is closed')
            # don't destroy here - it has already happened because someone closed the window
            self._fill = None

            self._close_other(self._key)

    def on_key_closed(self):
        if self._key is not None:
            app_log.debug('key is closed')
            # don't destroy here - it has already happened because someone closed the window
            self._key = None

            self._close_other(self._fill)

    def on_chroma_closed(self):
        if self._chroma is not None:
            app_log.debug('chroma is closed')
            # don't destroy here - it has already happened because someone closed the window
            self._chroma = None

            # Let VueJS know we are now closed
            self._eval_js('window.eventBus.$emit("window-closed", "chroma");')

    def on_app_closed(self):
        app_log.debug('app is closed')

        # Set flag to prevent normal on_close events firing
        # We want to tidy up nice and neatly here
        self._app_closing = True

        if self._fill is not None:
            self._fill.destroy()

        if self._key is not None:
            self._key.destroy()

        if self._chroma is not None:
            self._chroma.destroy()

    def on_open_clicked(self, element):
        if element == self.KEY_FILL:
            self._on_open_key_fill_clicked()
        elif element == self.CHROMA:
            self._on_open_chroma_clicked()

    def on_fullscreen_clicked(self, element):
        if element == self.KEY_FILL:
            self._toggle_key_fill_fullscreen()
        elif element == self.CHROMA:
            self._toggle_chroma_fullscreen()

    def on_upper_clicked(self, element, state):
        if element == self.KEY_FILL:
            self._key_fill_upper = state
            self.olp._update_slide()
        elif element == self.CHROMA:
            self._chroma_upper = state
            self.olp._update_slide()

    def on_visibility_clicked(self, element):
        if element == self.KEY_FILL:
            app_log.debug("Toggle key/fill visibility")
            self._key_hidden = not self._key_hidden
            self._fill_hidden = not self._fill_hidden
        elif element == self.CHROMA:
            app_log.debug("Toggle Chroma visibility")
            self._chroma_hidden = not self._chroma_hidden

        self._refresh_hidden_state()

    def getSystemFontList(self):
        fontnames = []
        hdc = win32gui.GetDC(None)
        win32gui.EnumFontFamilies(hdc, None, self._win32_font_callback, fontnames)
        win32gui.ReleaseDC(hdc, None)

        # Remove any fonts that have associated version beginning with @ since these are
        # fonts that can also be vertical and are likely not meaningful as a horizontal
        # text that this program can render???!
        remove_list = []
        fontnames.sort()
        for index, font in enumerate(fontnames):
            if font[0] == '@':
                fontnames[index] = font[1:]
                remove_list.append(fontnames[index])

        for item in remove_list:
            fontnames[:] = (value for value in fontnames if value != item)

        # Remove anything that looks like a font face variant from the name
        p = re.compile(r'\s(Semi|Extra|Ultra)?\s?(Black|Bold|Italic|Heavy|Medium|Light|Condensed|Narrow|Small|Demi)')
        for index, font in enumerate(fontnames):
            m = p.search(font)
            if m:
                s1 = m.span()[0]
                s2 = m.span()[1]
                fontnames[index] = font[:s1] + font[s2:]

        # Now assume that the shortest version of a name is the family name...
        # e.g. 'Casandra' and 'Casandra Light' are the same family so we only want one
        fontnames.sort()
        final_list = []
        current_base = fontnames[0]
        for index, font in enumerate(fontnames):
            if index > 0 and not font.startswith(current_base):
                final_list.append(current_base)
                current_base = font

        # Need to deal with final one separtely as we always worked one behind
        if(not fontnames[-1].startswith(current_base)):
            final_list.append(current_base)

        return json.dumps(final_list)

    # Private functions
    def _slide_updated(self, slide: Slide):
        app_log.debug("_slide_updated() %s", slide.type)
        if slide.type != self._last_slide_type:
            self._last_slide_type = slide.type
            app_log.debug("Sending new slide type %s", slide.type)
            msg = {
                "window_id": "key_fill",
                "action": "change_slide_type",
                "value": slide.type.name
            }

            self._set_key_fill_content()
            self._eval_js(
                "openlp_channel.postMessage(" + json.dumps(msg) + ");")

            msg['window_id'] = "chroma"
            self._set_chroma_content()
            self._eval_js(
                "openlp_channel.postMessage(" + json.dumps(msg) + ");")

        if slide.type == SlideType.BIBLE:
            stripped_nbsp = re.sub(
                r'sup\s*>&nbsp;', 'sup>', slide.cur_html, flags=re.I)
            stripped_all = re.sub(r'<\s*br\s*/?>', ' ',
                                  stripped_nbsp, flags=re.I)
            stripped_all = re.sub(
                r'<\s*sup\s*>\s*[0-9:\s]+<\s*/\s*sup\s*>', '', stripped_all, flags=re.I)

            self._set_key_fill_content(slide.bible_reference, stripped_all)
            self._set_chroma_content(slide.bible_header, stripped_all)
        else:
            if(slide.is_last):
                self._set_key_fill_content(slide.title, slide.cur_html, slide.footer_html)
                self._set_chroma_content(slide.title, slide.full_html, slide.footer_html)
            else:
                self._set_key_fill_content(slide.title, slide.cur_html)
                self._set_chroma_content(slide.title, slide.full_html)

    def _on_open_key_fill_clicked(self):
        reload = False
        if self._key is None:
            self._key = webview.create_window('Key', 'frontend/dist/key.html',
                                              js_api=self, background_color='#000')
            self._key.closed += self.on_key_closed
            self._refresh_hidden_state()
            reload = True

        if self._fill is None:
            self._fill = webview.create_window('Fill', 'frontend/dist/fill.html',
                                               js_api=self, background_color='#000')
            self._fill.closed += self.on_fill_closed
            self._refresh_hidden_state()
            reload = True

        if reload:
            # Set unknown slide type so that firing of update slide will work it out
            self._last_slide_type = SlideType.UNKNOWN
            self.olp._update_slide()

    def _on_open_chroma_clicked(self):
        if self._chroma is None:
            self._chroma = webview.create_window('Chroma', 'frontend/dist/chroma.html',
                                                 js_api=self, background_color='#0f0')
            self._chroma.closed += self.on_chroma_closed
            self._refresh_hidden_state()

        # Set unknown slide type so that firing of update slide will work it out
        self._last_slide_type = SlideType.UNKNOWN
        self.olp._update_slide()

    def _toggle_key_fill_fullscreen(self):
        app_log.debug("Toggle key/fill fullscreen")
        if self._key is not None:
            self._key.toggle_fullscreen()

        if self._fill is not None:
            self._fill.toggle_fullscreen()

    def _toggle_chroma_fullscreen(self):
        app_log.debug("Toggle chroma fullscreen")
        if self._chroma is not None:
            self._chroma.toggle_fullscreen()

    def _refresh_hidden_state(self):
        if self._fill is not None:
            self._fill.hide() if self._fill_hidden else self._fill.show()

        if self._key is not None:
            self._key.hide() if self._key_hidden else self._key.show()

        if self._chroma is not None:
            self._chroma.hide() if self._chroma_hidden else self._chroma.show()

    def _eval_js(self, cmd):
        if not self._app_closing:
            self._main_window.evaluate_js(cmd)

    def _close_other(self, other):
        if not self._app_closing and other is not None:
            other.destroy()
            other._key = None

            # Let VueJS know both are now closed
            self._eval_js(
                'window.eventBus.$emit("window-closed", "key_fill");')

    def _set_key_fill_content(self, title='', current='', footer=''):
        if self._key_fill_upper:
            title = title.upper()
            current = current.upper()

        title = title.rstrip()
        current = current.rstrip()

        app_log.debug("Sending JS to Key/Fill: %s | %s", title, current)
        msg = {
            "window_id": "key_fill",
            "action": "content",
            "value": {
                "title": title,
                "current": current,
                "footer": footer
            }
        }

        self._eval_js("openlp_channel.postMessage(" + json.dumps(msg) + ");")

    def _set_chroma_content(self, title='', current='', footer=''):
        if self._chroma_upper:
            title = title.upper()
            current = current.upper()

        title = title.rstrip()
        current = current.rstrip()

        app_log.debug("Sending JS to Chroma: %s | %s", title, current)
        msg = {
            "window_id": "chroma",
            "action": "content",
            "value": {
                "title": title,
                "current": current,
                "footer": footer
            }
        }

        self._eval_js("openlp_channel.postMessage(" + json.dumps(msg) + ");")

    def _win32_font_callback(self, font, tm, fonttype, names):
        if font.lfCharSet == 0 and font.lfOrientation == 0:  # < English charset, horizontal
            names.append(font.lfFaceName)
        return True


if __name__ == '__main__':
    arguments = docopt(__doc__, version='OpenLP Alternate Stage Screens')

    app_log.setLevel(logging.INFO)
    app_log.propagate = False

    # attach a console handler to toto_logger
    formatter = logging.Formatter(
        '%(asctime)s %(name)s %(levelname)s: %(message)s')
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    app_log.addHandler(console_handler)

    if arguments['--debug']:
        app_log.setLevel(logging.DEBUG)

    app_log.info("Starting main application...")
    api = MainApi()

    window = webview.create_window('OpenLP Stage Control', 'frontend/dist/index.html',
                                   js_api=api, background_color='#000', width=560, height=720)
    window.closed += api.on_app_closed
    api.set_main_window(window)
    webview.start(debug=arguments['--debug'])
    app_log.info("Exiting main application...")
