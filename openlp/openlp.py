import threading
import websocket
import gc
import re
import json
import time
import logging
import requests
from enum import Enum
import types

app_log = logging.getLogger(__name__)


class SlideType(Enum):
    BIBLE = 1
    SONG = 2
    OTHER = 3
    UNKNOWN = 4


class SlideState(Enum):
    HIDDEN = 0
    SHOWING = 1
    BLACK = 2
    THEME_ONLY = 3
    UNKNOWN = 4


class Slide:
    def __init__(self):
        self.title = None
        self.tag = None
        self.selected = False
        self.type = SlideType.UNKNOWN
        self.state = SlideState.UNKNOWN

        self.pre_text = None
        self.pre_html = None
        self.cur_text = None
        self.cur_html = None
        self.post_text = None
        self.post_html = None

        self.footer_html = None
        self.is_last = False

    @property
    def bible_header(self):
        header = ''

        if self.type == SlideType.BIBLE and self.title:
            result = re.match(r'((\d*\s)?[A-z]+\s+[\d:\-,\s]+)(.*)', self.title)
            if result:
                header = result.group(1) + '<br/>' + result.group(3)

        return header

    @property
    def bible_reference(self):
        name = ''

        if self.type == SlideType.BIBLE and self.title:
            result = re.match(r'((\d*\s)?[A-z]+\s+[\d:\-,\s]+)(.*)', self.title)
            if result:
                name = result.group(1)

        return name

    @property
    def full_text(self):
        return self.pre_text + self.cur_text + self.post_text

    @property
    def full_html(self):
        return self.pre_html + self.cur_html + self.post_html


class OpenLP:
    def __init__(self, host='127.0.0.1', http_port='4316', ws_port='4317'):
        self._host = host
        self._http_port = http_port
        self._ws_port = ws_port

        self._on_new_slide_cbks = []

        self._current_item_uid = None
        self._current_service = None
        self._current_slides = None
        self._current_slide_idx = None
        self._current_slides_type = SlideType.UNKNOWN
        self._current_tags = []

        self._current_slide_state = SlideState.UNKNOWN

        t1 = threading.Thread(target=self.__connect_websocket)
        t1.daemon = True
        t1.start()

    def registerSlideChangeCallback(self, callback: types.FunctionType):
        self._on_new_slide_cbks.append(callback)

    def __on_error(self, ws, error):
        app_log.debug(error)

    def __on_close(self, ws):
        app_log.debug("### closed ###")

    def __on_open(self, ws):
        app_log.debug("### opened ###")
        self._load_service()

    def __on_message(self, ws, message):
        app_log.debug("Enter - __on_message()")
        data = json.loads(message)['results']

        app_log.debug("Data: %s", data)
        if self._current_item_uid != data['item'] or self._current_service != data['service']:
            self._current_item_uid = data['item']
            self._current_service = data['service']
            self._load_slides()
        elif self._current_slide_idx != data['slide']:
            self._current_slide_idx = data['slide']

        self._current_slide_state = self.__slide_state(data)
        self._update_slide()
        app_log.debug("Exit - __on_message()")

    def __connect_websocket(self):
        while True:
            try:
                ws = websocket.WebSocketApp(f"ws://{self._host}:{self._ws_port}",
                                            on_message=self.__on_message,
                                            on_error=self.__on_error,
                                            on_close=self.__on_close,
                                            on_open=self.__on_open)
                ws.run_forever(skip_utf8_validation=True, ping_interval=10, ping_timeout=8)
            except Exception as e:
                gc.collect()
                app_log.debug("Websocket connection Error  : {0}".format(e))
            app_log.debug("Reconnecting websocket  after 5 sec")
            time.sleep(5)

    def _load_service(self):
        app_log.debug('Enter - _load_service()')
        r = requests.get(f'http://{self._host}:{self._http_port}/api/v2/service/items')
        self._service = r.json()

        self._load_slides()
        self._update_slide()
        app_log.debug('Exit - _load_service()')

    def _load_slides(self):
        app_log.debug('ENTER - _load_slides()')
        r = requests.get(f'http://{self._host}:{self._http_port}/api/v2/controller/live-items')
        data = r.json()
        # app_log.debug(data)
        app_log.debug("Slides are type: %s", data['name'])

        self._previous_slides_type = self._current_slides_type
        self._current_slides_type = self.__slides_type(data)

        self._current_slides = data['slides']
        self._current_slide_idx = 0
        self._current_tags = []

        tag = ""
        tags = 0
        lastChange = 0

        for idx, slide in enumerate(data['slides']):
            prevtag = tag
            tag = slide['tag']

            if tag != prevtag:
                lastChange = idx
                tags = tags + 1
            elif slide['text'] == data['slides'][lastChange]["text"] and len(data['slides']) >= idx + (idx - lastChange):
                # If the tag hasn't changed, check to see if the same verse
                # has been repeated consecutively. Note the verse may have been
                # split over several slides, so search through. If so, repeat the tag.
                match = True
                for idx2 in range(idx - lastChange):
                    if data['slides'][lastChange + idx2]['text'] != data['slides'][idx + idx2]['text']:
                        match = False
                        break

                if match:
                    lastChange = idx
                    tags = tags + 1

            self._current_tags.insert(idx, tags)

            if slide['selected']:
                self._current_slide_idx = idx

        app_log.debug('EXIT - _load_slides()')

    def __slide_state(self, data):
        if data['blank']:
            return SlideState.BLACK
        elif data['theme']:
            return SlideState.THEME_ONLY
        elif data['display']:
            return SlideState.HIDDEN

        return SlideState.SHOWING

    def __slides_type(self, data):
        if data['name'] == 'bibles':
            return SlideType.BIBLE
        elif data['name'] == 'songs':
            return SlideType.SONG

        return SlideType.OTHER

    def __previous_content(self):
        currtag = self._current_tags[self._current_slide_idx]
        start = 0
        prev = ""
        prev_html = ""

        if self._current_slide_idx > 0:
            for idx in range(self._current_slide_idx, -1, -1):
                if self._current_tags[idx] != currtag:
                    start = idx + 1
                    break

            for idx in range(start, self._current_slide_idx):
                if self._current_slides[idx]["text"]:
                    prev += self._current_slides[idx]["text"]

                if self._current_slides[idx]["html"]:
                    prev_html += self._current_slides[idx]["html"]

                prev += "\n"
                prev_html += "<br/>"
        return prev, prev_html

    def __current_content(self):
        slide = self._current_slides[self._current_slide_idx]

        curr = slide["text"] + "\n" if slide["text"] else ''
        curr_html = slide["html"] + "<br/>" if slide["html"] else ''

        return curr, curr_html

    def __post_content(self):
        currtag = self._current_tags[self._current_slide_idx]
        next = ''
        next_html = ''

        if self._current_slide_idx < len(self._current_slides) - 1:
            for idx in range(self._current_slide_idx + 1, len(self._current_slides)):
                if self._current_tags[idx] == currtag and self._current_slides[idx]["text"] is not None:
                    next += self._current_slides[idx]["text"]
                    next += "\n"

                if self._current_tags[idx] == currtag and self._current_slides[idx]["html"] is not None:
                    next_html += self._current_slides[idx]["html"]
                    next_html += "<br/>"

        return next, next_html

    def _update_slide(self):
        app_log.debug('Enter - _update_slide()')
        try:
            slide = self._current_slides[self._current_slide_idx]

            s = Slide()
            s.title = slide['title']
            s.tag = slide['tag']
            s.footer_html = slide['footer']
            s.selected = slide['selected']
            s.pre_text, s.pre_html = self.__previous_content()
            s.cur_text, s.cur_html = self.__current_content()
            s.post_text, s.post_html = self.__post_content()
            s.type = self._current_slides_type
            s.state = self._current_slide_state
            s.is_last = (self._current_slide_idx == (len(self._current_slides) - 1))

            for callback in self._on_new_slide_cbks:
                callback(s)
        except IndexError as error:
            app_log.exception(error)

        app_log.debug('Exit - _update_slide()')
